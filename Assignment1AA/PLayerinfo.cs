﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1AA
{
    public class PLayerinfo
    {
        //information of the players applying for the soccer team
        // firstname
        // lastname
        // schoolname

        private string playerFirstName;
        private string playerLastName;
        private string playerSchoolName;
  

        public PLayerinfo ()
        {

        }
        public string PlayerFirstName
        {
            get { return playerFirstName; }
            set { playerFirstName = value; }
        }
        public string PlayerLastName
        {
            get { return playerLastName; }
            set { playerLastName = value; }
        }
        public string PlayerSchoolName
        {
            get { return playerSchoolName; }
            set { playerSchoolName = value; }
        }


    }
}