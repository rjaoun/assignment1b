﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1AA
{
    public class ApplicantSummary
    {
        public PLayerinfo playerinfo;
        public Playerstat playerstat;
        

        public ApplicantSummary(PLayerinfo pi, Playerstat ps)
        {
            playerinfo = pi;
            playerstat = ps;
        }

        public string ApplicationSummary()
        {
            string summary = "based on the information provided <br>" + "<br>";
            summary += "Your name is " +"<em><strong>" +"[" + playerinfo.PlayerFirstName  +"]" + "</strong></em>"+ " " + "<em><strong>"+"[" + playerinfo.PlayerLastName +"]" +"</em></strong>" + "<br><br>";
            summary += "Your age is: " + "<em><strong>" + "[" + playerstat.Age + "</em></strong>" + " which " + "<em><strong>" + TeamAllowance() + GenderTeam() +"]"+ "</em></strong>" + "<br><br>";
            summary += "you will be playing as " + "<em><strong>" + "[" + String.Join(" and ", playerstat.Positions.ToArray()) + "]" + "</em></strong>" + "<br><br>";
            summary += "Practing time chosen is " + "<em><strong>" + "[" + String.Join(" and ", playerstat.PraticeTimes.ToArray()) + "]" + "</em></strong>" + "<br><br>";
            summary += "Your Experience is: " + "<em><strong>" + "[" + playerstat.Experience + "]" +"</em></strong>" + "<br><br>";
             
            return summary;
        }

        public string TeamAllowance()
        {
            string information;
            if(playerstat.Age < 12)
            {
                information = "allows you to enter the kids team (8 - 11) ";
            }
            else if(playerstat.Age>=12 && playerstat.Age < 18)
            {
                information = "allows you to enter the mid team ( 12 - 17) ";
            }
            else
            {
                information = "allows you to enter the mature team ( 18 - 25)";
            }
            return information;
        }
        public string GenderTeam()
        {
            string gendertype;
            if(playerstat.Gender =="Male")
            {
                gendertype = " in the Male team";
            }
            else
            {
                gendertype = " in the Female team";
            }
            return gendertype;
        }


    }
}