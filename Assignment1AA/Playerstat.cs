﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1AA
{
    public class Playerstat
    {
        public int Age;

        public string Gender;

        public List<string> PraticeTimes;

        public List<string> Positions;

        public string Experience;

        public Playerstat(int a, string g, List<string> p, List<string> po, string e)
        {
            Age = a;
            Gender = g;
            PraticeTimes = p;
            Positions = po;
            Experience = e;
        }
    }
}