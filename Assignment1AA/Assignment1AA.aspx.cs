﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1AA
{
    public partial class Assignment1AA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        /* Examination Validator to validate that if the applicant selected Yes as he has allergies which means applicant should
        Go through medical examinations.*/
        protected void Examination_Validator(object source, ServerValidateEventArgs args)
        {
            if (allergy.SelectedValue == "Yes")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        // This is the code we will be running after pressing submit.
        protected void ApplicantSummary(object sender, EventArgs e)
        {
            // Checking if all validations are valid
            if (!Page.IsValid)
            {
                return;
            }
            

            /*
             * Creating the PLayer information object
             *  In Playerinfo.cs we will have applicant first name,
             *  applicant last name, and school name.
             */
            // Applicant first name as a string

            string firstname = applicantfirstName.Text.ToString();
            // Applicant last name as a string
            string lastname = applicantlastName.Text.ToString();
            // school name as a string
            string schoolname = schoolName.Text.ToString();

            PLayerinfo newplayer = new PLayerinfo();
            newplayer.PlayerFirstName = firstname;
            newplayer.PlayerLastName = lastname;
            newplayer.PlayerSchoolName = schoolname;

            /*
            * Creating Player Stat objects which determines
            * Which team the applicant will be joining whether male or female team
            * depending on the age if they will be joining kid, mid, or mature team.
            * Also practice time and positioning will be requested to check what best suites you.
            */


            // Applicant age as an integer
            int Age = int.Parse(applicantage.Text);
            
            // gender as a string
            string Gender = gender.Text.ToString();

            // practice time as list of strings

            List<string> PracticeTimes = new List<string>();


            // foreach loop to get the values of the PracticeTime which will be put in array as Text
            // This code was taken from Christine code of Source Control code.
            foreach (Control control in practiceTime.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox time = (CheckBox)control;
                    if (time.Checked)
                    {
                        PracticeTimes.Add(time.Text);
                    }

                }
            }


            // position as list of strings

            List<string> Positions = new List<string>();
            
            foreach (Control control in position.Controls)
            {
                if( control.GetType() == typeof(CheckBox))
                {
                    CheckBox positioning = (CheckBox)control;
                    if (positioning.Checked)
                    {
                        Positions.Add(positioning.Text);
                    }
                }
            }
           
            // experience part
            string Experience = experience.SelectedItem.Value.ToString();

            Playerstat newapplicant = new Playerstat(Age, Gender, PracticeTimes, Positions, Experience);

            ApplicantSummary newApplicant = new ApplicantSummary(newplayer, newapplicant);

            applicantStat.InnerHtml = newApplicant.ApplicationSummary();

        }
    }
}