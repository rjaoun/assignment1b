﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1AA.aspx.cs" Inherits="Assignment1AA.Assignment1AA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>              
                <legend style="text-align: center; font-weight: bold; font-size: 20px;">Soccer Team Application</legend>

                    <div id="info" runat="server">

                    </div>
<!-- Label with TextBox for First Name with RequiredFieldValidator to make sure user entered the first name -->
                    <label>First Name: </label>
                    <br />
                    <asp:TextBox ID="applicantfirstName" runat="server" placeholder="first name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="firstNameValidator" runat="server"
                        ControlToValidate="applicantfirstName" ErrorMessage="Please Type your First Name" />
                    <br />
 <!-- Label with TextBox for Last Name with RequiredFieldValidator to make sure user entered the last name -->               
                    <label>Last Name: </label>
                    <br />
                    <asp:TextBox ID="applicantlastName" runat="server" placeholder="last name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="lastNameValidator" runat="server"
                        ControlToValidate="applicantlastName" ErrorMessage="Please Type your Last Name" />
                    <br />

 <!-- Label with TextBox for School Name with RequiredFieldValidator to make sure user entered the school name -->            
                    <label>School Name: </label>
                    <br />
                    <asp:TextBox ID="schoolName" runat="server" placeholder="school name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="schoolNameValidator" runat="server"
                        ControlToValidate="schoolName" ErrorMessage="Please Type your School Name" />
                    <br />

 <!-- Label with TextBox for password-->
                    <!--<asp:Label ID="Password" runat="server">Password: </asp:Label>
                    <br />
                    <asp:TextBox ID="pass" runat="server" TextMode="Password"></asp:TextBox>
                    <br /> -->

 <!-- Label with TextBox for confirming the password match using CompareValidator -->
                    <!-- <asp:Label ID="CPassword" runat="server">Confirm Password: </asp:Label>
                    <br />
                    <asp:TextBox ID="cpass" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ErrorMessage="Password and confirm Password does not match."
                        ControlToValidate="cpass" runat="server"></asp:CompareValidator>
                    <br /> -->
 <!-- Label with TextBox for Phone number with RegularExpressionValidator to make sure
      the phone number format is (000)000 - 0000  -->            
                    <!--<asp:Label runat="server">Phone Number: </asp:Label>
                    <br />
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator 
                        ID="regPhone" 
                        runat="server" 
                        ControlToValidate="txtPhone" 
                        ValidationExpression="^(\(?\s*\d{3}\s*[\)\-\.]?\s*)?[2-9]\d{2}\s*[\-\.]\s*\d{4}$"
                        Text="Enter a valid phone number" />
                    <br /> -->

 <!-- Label with two RadioButtons to choose the age -->            
                    <label>Age: </label>
                    <br />
                    <asp:TextBox ID="applicantage" runat="server" placeholder="Age"></asp:TextBox>
                    <asp:RangeValidator runat="server" ControlToValidate="applicantage" Type="Integer" MinimumValue="8" MaximumValue="25" ErrorMessage="You should be between the age of 8 and 15 to enter this team"></asp:RangeValidator>
            <br />
    
 <!-- Label with three RadioButtons to choose the gender -->                
                    <label>Gender: </label>
                    <br />
                    <asp:RadioButtonList runat="server" ID="gender">
                        <asp:ListItem Text="Male">Male</asp:ListItem>
                        <asp:ListItem Text="Female">Female</asp:ListItem>
                    </asp:RadioButtonList>
                   
                    <br />
 <!-- Label with Three CheckBoxes to choose the best time for practing -->  
                <!-- ID used is "practiceTime" -->
                    <label>Practice Time: </label>
                    <br />
                    <div id="practiceTime" runat="server">
                        <asp:CheckBox ID="Morning" value="morning" runat="server" Text="Morning"/>
                        <asp:CheckBox ID="Evening" value="evening" runat="server" Text="Evening"/>
                        <asp:CheckBox ID="Afternoon" value="afternoon" runat="server" Text="Afternoon"/>
                    </div>
                    <br />

<!-- Label with 4 CheckBoxes to choose types of position the user plays -->
                <!-- ID used is "position" -->
                    <label>Position: </label>
                    <br />
                    <div id="position" runat="server">
                        <asp:CheckBox ID="GoalKeeper" value="goalkeeper" runat="server" Text="Goal Keeper" />
                        <asp:CheckBox ID="Defender" value="defender" runat="server" Text="Defender" />
                        <asp:CheckBox ID="Attacker" value="attacker" runat="server" Text="Attacker" />
                        <asp:CheckBox ID="Midfielder" value="midfielder" runat="server" Text="Midfielder" />
                    </div>
                    <br />

 <!-- Label with DropDownList to choose player experience -->
                <!-- ID used is "experience" -->
                    <label>Experience: </label>
                    <br />
                    
                    <asp:DropDownList ID="experience" runat="server" >  
                    <asp:ListItem Value="Beginner" Selected="True">Beginner </asp:ListItem>              
                    <asp:ListItem Value="Intermediate">Intermediate </asp:ListItem>  
                    <asp:ListItem Value="Advanced">Advanced</asp:ListItem>
                    <asp:ListItem Value="Expert">Expert</asp:ListItem>                
                    </asp:DropDownList> 
                    <br />

<!-- Label with 2 RadioButtons for Allergies checking -->
                <!-- ID used is "allergy" -->
                    <label>Allergies: </label>
                    <br />
                    <asp:RadioButtonList ID="allergy" runat="server">
                        <asp:ListItem Text="Yes">Yes</asp:ListItem>
                        <asp:ListItem Text="No">No</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:CustomValidator runat="server" ErrorMessage="You are required to do medical exam first before applying!." ControlToValidate="allergy" OnServerValidate="Examination_Validator"></asp:CustomValidator>
                    <br />
                

<!-- Submit button -->                 
                    <asp:Button ID="myButton" runat="server" Text="Submit" BorderStyle="Solid" OnClick="ApplicantSummary"/>
                    
                    <asp:ValidationSummary ForeColor="Red" runat="server" ID="validationSummary" /> 
                 </fieldset>   
            </div>
                    
                    <div id="applicantStat" runat="server" style="background-color: gray; text-align: center;">


                    </div>
            

    </form>
</body>
</html>
